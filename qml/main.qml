import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4

ApplicationWindow {
	id: windowItem
	width: 1200
	height: 800
	visible: true
	title: "qmltest"
	style: ApplicationWindowStyle {
    	background: Rectangle { color: "#FFFFFF"; anchors.fill: parent; }
    }

    function ptToPx(pt) {
        return Screen.pixelDensity * 25.4 * Math.round(pt) / 72
    }

    function pxToPt(px) {
        return Math.round(px) * 72 / (Screen.pixelDensity * 25.4)
    }

	ColumnLayout {
		id: col
		property real scale: 1.5

		Text { text: "Screen.pixelDensity: " + Screen.pixelDensity + ", col.scale: " + col.scale}
		Slider {
			id: zoomSlider
			minimumValue: 0.5
			maximumValue: 4.0
			value: col.scale
			updateValueWhileDragging: true
			onValueChanged: {
				col.scale = zoomSlider.value
			}
			width: 400
		}
		TestRow { realbase: 10 * col.scale }
		TestRow { realbase: 16 * col.scale }
		TestRow { realbase: 24 * col.scale }
		TestRow { realbase: 36 * col.scale }
		TestRow { realbase: 48 * col.scale }
	}
}