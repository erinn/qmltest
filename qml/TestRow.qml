import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4

ColumnLayout {
	id: sec
	property real realbase: 10.0
	property int intbase: realbase

	RowLayout {
		Text {
			font.pointSize: sec.intbase
			text: "int pointSize: " + sec.intbase
		}

		TextField {
			text: "pt" + sec.intbase + "; ptToPx("+sec.intbase+")"
			style: TextFieldStyle {
				id: root
				textColor: "black"
				font.pointSize: sec.intbase
				property int width: 500

				background: Rectangle {
					radius: 2
					//implicitWidth: root.width
					//implicitHeight: ptToPx(sec.intbase)
					color: "#DDDDFF"
					border.color: "#000000"
				}
			}
		}
	}

	RowLayout {
		Text {
			font.pointSize: sec.realbase
			text: "real pointSize: " + sec.realbase
		}

		TextField {
			text: "pt" + sec.realbase + "; ptToPx("+sec.realbase+")"
			style: TextFieldStyle {
				id: root
				textColor: "black"
				font.pointSize: sec.realbase
				property int width: 500

				background: Rectangle {
					radius: 2
					//implicitWidth: root.width
					//implicitHeight: ptToPx(sec.realbase)
					color: "#DDDDFF"
					border.color: "#000000"
				}
			}
		}
	}
}