package main

import (
	"flag"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quickcontrols2"
	"os"
)

type Thingama struct {
	core.QObject

	_ string `property:"x"`
	_ core.QStringList `property:"mahdahl"`
	_ core.QAbstractItemModel `property:"abstracty"`

	_ func() `signal:"doTheThing,auto"`
}

func (this *Thingama) doTheThing() {
}

func main() {
	flagDebug := flag.Bool("debug", false, "turn on extra logging. WARNING: THIS MAY EXPOSE PRIVATE INFORMATION IN CONSOLE OUTPUT!")
	flagLocal := flag.Bool("local", false, "load user interface from the local folder \"qml\" instead of the built-in UI")
	flag.Parse()

	if *flagDebug {
		log.SetLevel(log.LevelDebug)
	} else {
		log.SetLevel(log.LevelInfo)
	}

	// this is to load local qml files quickly when developing
	var qmlSource *core.QUrl
	if *flagLocal {
		qmlSource = core.QUrl_FromLocalFile("./qml/main.qml")
	} else {
		qmlSource = core.NewQUrl3("qrc:/qml/main.qml", 0)
	}

	core.QCoreApplication_SetAttribute(core.Qt__AA_Use96Dpi, true)
	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, false)
	app := gui.NewQGuiApplication(len(os.Args), os.Args)
	//QApplication::setAttribute(Qt::AA_Use96Dpi);
	quickcontrols2.QQuickStyle_SetStyle("Default")
	engine := qml.NewQQmlApplicationEngine(nil)

	thingama := NewThingama(nil)
	thingama.SetX("hello")
	engine.RootContext().SetContextProperty("thingama", thingama)

	engine.Load(qmlSource)
	app.Exec()
}
