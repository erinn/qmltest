module git.openprivacy.ca/erinn/qmltest

go 1.12

require (
	git.openprivacy.ca/openprivacy/libricochet-go v1.0.6
	github.com/therecipe/qt v0.0.0-20190914193131-4c875570321e
)
